﻿namespace Notrello.DAL
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    using Notrello.DAL.Model;

    public class BoardContext : DbContext
    {
        static BoardContext()
        {
            Database.SetInitializer<BoardContext>(new BoardContextInitializer());
        }

        public BoardContext() : base("BoardDataBaseConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // ***Tables setup***
            modelBuilder.Entity<CardEntity>().ToTable("Cards")
                .HasKey(card => card.Id)
                .Property(card => card.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ListEntity>().ToTable("Lists")
                .HasKey(list => list.Id)
                .Property(list => list.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<BoardEntity>().ToTable("Boards")
                .HasKey(board => board.Id)
                .Property(board => board.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // ***Relationships setup***
            modelBuilder.Entity<BoardEntity>()
                .HasMany(board => board.Lists)
                .WithRequired(list => list.Board)
                .HasForeignKey(list => list.BoardId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ListEntity>()
                .HasMany(list => list.Cards)
                .WithRequired(card => card.List)
                .HasForeignKey(card => card.ListId)
                .WillCascadeOnDelete(true);

            // ***Tables' fields setup***
            modelBuilder.Entity<CardEntity>()
                .Property(card => card.Text).IsRequired();

            modelBuilder.Entity<ListEntity>()
                .Property(list => list.Name).IsRequired();

            modelBuilder.Entity<BoardEntity>()
                .Property(board => board.Name).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
