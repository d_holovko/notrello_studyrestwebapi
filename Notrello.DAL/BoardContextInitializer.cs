﻿namespace Notrello.DAL
{
    using System.Data.Entity;
    using System.Linq;

    using Bogus;

    using Notrello.DAL.Model;

    public class BoardContextInitializer : DropCreateDatabaseIfModelChanges<BoardContext>
    {
        protected override void Seed(BoardContext context)
        {
            var fakeCardsGenerator = new Faker<CardEntity>()
                .RuleFor(card => card.Text, f => f.Lorem.Sentence(3, 7));

            var fakeListsGenerator = new Faker<ListEntity>()
                .RuleFor(list => list.Name, f => f.Lorem.Word())
                .RuleFor(list => list.Cards, f => fakeCardsGenerator.Generate(4).ToList());

            var fakeBoardsGenerator = new Faker<BoardEntity>()
                .RuleFor(board => board.Name, f => f.Lorem.Word())
                .RuleFor(board => board.Lists, f => fakeListsGenerator.Generate(3));

            var fakeBoards = fakeBoardsGenerator.Generate(5);

            context.Set<BoardEntity>().AddRange(fakeBoards);

            context.SaveChanges();
        }
    }
}