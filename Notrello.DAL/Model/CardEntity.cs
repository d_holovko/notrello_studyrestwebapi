﻿namespace Notrello.DAL.Model
{
    using System;

    public class CardEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public ListEntity List { get; set; }

        public Guid ListId { get; set; }
    }
}
