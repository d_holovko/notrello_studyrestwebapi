﻿namespace Notrello.DAL.Model
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
