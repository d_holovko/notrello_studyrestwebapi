﻿namespace Notrello.DAL.Model
{
    using System;
    using System.Collections.Generic;

    public class ListEntity : IEntity<Guid>
    {
        public ListEntity()
        {
            Cards = new HashSet<CardEntity>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<CardEntity> Cards { get; set; }

        public BoardEntity Board { get; set; }

        public Guid BoardId { get; set; }
    }
}
