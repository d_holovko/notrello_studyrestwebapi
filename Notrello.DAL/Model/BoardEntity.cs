﻿namespace Notrello.DAL.Model
{
    using System;
    using System.Collections.Generic;

    public class BoardEntity : IEntity<Guid>
    {
        public BoardEntity()
        {
            Lists = new HashSet<ListEntity>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<ListEntity> Lists { get; set; }
    }
}