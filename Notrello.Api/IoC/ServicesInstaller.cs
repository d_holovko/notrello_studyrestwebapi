﻿namespace Notrello.Api.IoC
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Notrello.BLL.Services;
    using Notrello.BLL.Services.Implementation;

    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IBoardService>().ImplementedBy<BoardService>().LifestylePerWebRequest(),
                Component.For<IListService>().ImplementedBy<ListService>().LifestylePerWebRequest(),
                Component.For<ICardService>().ImplementedBy<CardService>().LifestylePerWebRequest()
            );
        }
    }
}