﻿namespace Notrello.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Core;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http;
    using System.Web.UI.WebControls;

    using FluentValidation;
    using FluentValidation.Results;

    using Notrello.BLL.Services;
    using Notrello.DAL.Model;
    using Notrello.Validation;

    public class BoardController : ApiController
    {
        private const string UNEXPECTED_ERROR_RESPONSE = "Unexpected error occured.";

        private const string DEFAULT_ID_RESPONSE = "Default id value is not allowed.";

        private const string INVALID_MODEL_RESPONSE = "Model is not valid.";

        private readonly IBoardService boardService;

        private readonly IListService listService;

        private readonly ICardService cardService;

        public BoardController(IBoardService boardService, IListService listService, ICardService cardService)
        {
            this.boardService = boardService;
            this.listService = listService;
            this.cardService = cardService;
        }

        // ***Boards features***
        [HttpGet]
        [Route("v1/boards")]
        public HttpResponseMessage GetBoards()
        {
            try
            {
                var boards = boardService.Read();

                return Request.CreateResponse(HttpStatusCode.OK, boards);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet]
        [Route("v1/board/{id:Guid}")]
        public HttpResponseMessage GetBoard(Guid id)
        {
            if (id == Guid.Empty)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, DEFAULT_ID_RESPONSE);
            }

            try
            {
                var board = boardService.Read(id);

                return Request.CreateResponse(HttpStatusCode.OK, board);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpPost]
        [Route("v1/board")]
        public HttpResponseMessage PostBoard(BoardEntity board)
        {
            if (board == null)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, INVALID_MODEL_RESPONSE);
            }

            var validator = new BoardValidator();

            var validation = validator.Validate(board, ruleSet: "Creation");

            if (!validation.IsValid)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, validation.Errors, INVALID_MODEL_RESPONSE);
            }

            try
            {
                boardService.Create(board);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpPut]
        [Route("v1/board")]
        public HttpResponseMessage PutBoard(BoardEntity board)
        {
            if (board == null)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, INVALID_MODEL_RESPONSE);
            }

            var validator = new BoardValidator();

            var validation = validator.Validate(board, ruleSet: "Full");

            if (!validation.IsValid)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, validation.Errors, INVALID_MODEL_RESPONSE);
            }

            try
            {
                boardService.Update(board);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpDelete]
        [Route("v1/board/{id:Guid}")]
        public HttpResponseMessage DeleteBoard(Guid id)
        {
            if (id == Guid.Empty)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, DEFAULT_ID_RESPONSE);
            }

            try
            {
                boardService.Delete(id);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        // ***Lists features***
        [HttpGet]
        [Route("v1/lists")]
        public HttpResponseMessage GetLists()
        {
            try
            {
                var lists = listService.Read();

                return Request.CreateResponse(HttpStatusCode.OK, lists);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet]
        [Route("v1/list/{id:Guid}")]
        public HttpResponseMessage GetList(Guid id)
        {
            if (id == Guid.Empty)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, DEFAULT_ID_RESPONSE);
            }

            try
            {
                var list = listService.Read(id);

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpPost]
        [Route("v1/list")]
        public HttpResponseMessage PostList(ListEntity list)
        {
            if (list == null)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, INVALID_MODEL_RESPONSE);
            }

            var validator = new ListValidator();

            var validation = validator.Validate(list, ruleSet: "Creation");

            if (!validation.IsValid)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, validation.Errors, INVALID_MODEL_RESPONSE);
            }

            try
            {
                listService.Create(list);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpPut]
        [Route("v1/list")]
        public HttpResponseMessage PutList(ListEntity list)
        {
            if (list == null)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, INVALID_MODEL_RESPONSE);
            }

            var validator = new ListValidator();

            var validation = validator.Validate(list, ruleSet: "Full");

            if (!validation.IsValid)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, validation.Errors, INVALID_MODEL_RESPONSE);
            }

            try
            {
                listService.Update(list);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpDelete]
        [Route("v1/list/{id:Guid}")]
        public HttpResponseMessage DeleteList(Guid id)
        {
            if (id == Guid.Empty)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, DEFAULT_ID_RESPONSE);
            }

            try
            {
                listService.Delete(id);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        // ***Card features***
        [HttpGet]
        [Route("v1/cards")]
        public HttpResponseMessage GetCards()
        {
            try
            {
                var cards = cardService.Read();

                return Request.CreateResponse(HttpStatusCode.OK, cards);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception);
            }
        }

        [HttpGet]
        [Route("v1/card/{id:Guid}")]
        public HttpResponseMessage GetCard(Guid id)
        {
            if (id == Guid.Empty)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, DEFAULT_ID_RESPONSE);
            }

            try
            {
                var card = cardService.Read(id);

                return Request.CreateResponse(HttpStatusCode.OK, card);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpPost]
        [Route("v1/card")]
        public HttpResponseMessage PostCard(CardEntity card)
        {
            if (card == null)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, INVALID_MODEL_RESPONSE);
            }

            var validator = new CardValidator();

            var validation = validator.Validate(card, ruleSet: "Creation");

            if (!validation.IsValid)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, validation.Errors, INVALID_MODEL_RESPONSE);
            }

            try
            {
                cardService.Create(card);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpPut]
        [Route("v1/card")]
        public HttpResponseMessage PutCard(CardEntity card)
        {
            if (card == null)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, INVALID_MODEL_RESPONSE);
            }

            var validator = new CardValidator();

            var validation = validator.Validate(card, ruleSet: "Full");

            if (!validation.IsValid)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, validation.Errors, INVALID_MODEL_RESPONSE);
            }

            try
            {
                cardService.Update(card);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        [HttpDelete]
        [Route("v1/card/{id:Guid}")]
        public HttpResponseMessage DeleteCard(Guid id)
        {
            if (id == Guid.Empty)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, DEFAULT_ID_RESPONSE);
            }

            try
            {
                cardService.Delete(id);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ArgumentNullException exception)
            {
                return CreateResponseMessage(HttpStatusCode.BadRequest, exception.Message);
            }
            catch (ObjectNotFoundException exception)
            {
                return CreateResponseMessage(HttpStatusCode.NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return CreateResponseMessage(HttpStatusCode.InternalServerError, UNEXPECTED_ERROR_RESPONSE);
            }
        }

        // Service methods
        [NonAction]
        private HttpResponseMessage CreateResponseMessage(HttpStatusCode statusCode, string reason)
        {
            var response = Request.CreateResponse(statusCode);
            response.ReasonPhrase = reason;

            return response;
        }

        [NonAction]
        private HttpResponseMessage CreateResponseMessage(HttpStatusCode statusCode, IEnumerable<ValidationFailure> validationFailures, string reason)
        {
            var errors = new StringBuilder();

            foreach (var failure in validationFailures)
            {
                errors.AppendLine($" {failure.PropertyName} : {failure.ErrorMessage}");
            }

            var response = Request.CreateResponse(statusCode, errors.ToString());
            response.ReasonPhrase = reason;

            return response;
        }
    }
}
