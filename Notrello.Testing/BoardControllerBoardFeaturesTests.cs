﻿namespace Notrello.Testing
{
    using System;
    using System.Data.Entity.Core;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using Notrello.Api.Controllers;
    using Notrello.BLL.Services;

    using NUnit.Framework;

    using Rhino.Mocks;

    [TestFixture]
    public class BoardControllerBoardFeaturesTests
    {
        private IBoardService boardService;

        private IListService listService;

        private ICardService cardService;

        private BoardController controller;

        [SetUp]
        public void SetUp()
        {
            boardService = MockRepository.GenerateMock<IBoardService>();
            listService = MockRepository.GenerateMock<IListService>();
            cardService = MockRepository.GenerateMock<ICardService>();

            controller = new BoardController(boardService, listService, cardService);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
        }

        [Test]
        public void GetBoards_WhenCalled_BoardServiceReadWasCalled()
        {
            // act
            controller.GetBoards();

            // assert
            boardService.AssertWasCalled(bs => bs.Read());
        }

        [Test]
        public void GetBoards_WhenCalledButNoBoardEntityExists_ObjectNotFoundExceptionCaughtAndStatusCode404Returned()
        {
            // arrange
            boardService.Stub(bs => bs.Read()).Throw(new ObjectNotFoundException());

            // act
            var response = controller.GetBoards();

            // assert
            Assert.That(response.StatusCode == HttpStatusCode.NotFound);
        }

        [Test]
        public void GetBoards_WhenCalledButUnexpectedErrorOccured_ExceptionCaughtAndStatusCode500Returned()
        {
            // arrange
            boardService.Stub(bs => bs.Read()).Throw(new Exception());

            // act
            var response = controller.GetBoards();

            // assert
            Assert.That(response.StatusCode == HttpStatusCode.InternalServerError);
        }

        [Test]
        [TestCase("00000001-0001-0001-0001-000000000001", HttpStatusCode.OK)]
        [TestCase("00000000-0000-0000-0000-000000000000", HttpStatusCode.BadRequest)]
        [TestCase("00000002-0002-0002-0002-000000000002", HttpStatusCode.BadRequest)]
        [TestCase("00000003-0003-0003-0003-000000000003", HttpStatusCode.NotFound)]
        [TestCase("00000004-0004-0004-0004-000000000004", HttpStatusCode.InternalServerError)]
        public void GetBoard(string id, HttpStatusCode expectedResult)
        {
            // arrange
            boardService.Stub(bs => bs.Read(Arg<Guid>.Is.Equal(Guid.Parse("00000002-0002-0002-0002-000000000002")))).Throw(new ArgumentNullException());
            boardService.Stub(bs => bs.Read(Arg<Guid>.Is.Equal(Guid.Parse("00000003-0003-0003-0003-000000000003")))).Throw(new ObjectNotFoundException());
            boardService.Stub(bs => bs.Read(Arg<Guid>.Is.Equal(Guid.Parse("00000004-0004-0004-0004-000000000004")))).Throw(new Exception());

            // act
            var response = controller.GetBoard(Guid.Parse(id));

            // assert
            Assert.That(response.StatusCode == expectedResult);
        }
    }
}
