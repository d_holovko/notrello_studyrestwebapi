﻿namespace Notrello.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using Notrello.DAL.Model;

    public interface IListService
    {
        void Create(ListEntity list);

        ListEntity Read(Guid id);

        IEnumerable<ListEntity> Read();

        void Update(ListEntity list);

        void Delete(Guid id);

        void Delete(ListEntity list);
    }
}
