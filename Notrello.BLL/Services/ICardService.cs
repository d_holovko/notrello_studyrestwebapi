﻿namespace Notrello.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using Notrello.DAL.Model;

    public interface ICardService
    {
        void Create(CardEntity card);

        CardEntity Read(Guid id);

        IEnumerable<CardEntity> Read();

        void Update(CardEntity card);

        void Delete(Guid id);

        void Delete(CardEntity card);
    }
}
