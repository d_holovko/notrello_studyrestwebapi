﻿namespace Notrello.BLL.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core;
    using System.Linq;

    using Notrello.DAL;
    using Notrello.DAL.Model;

    public class ListService : IListService
    {
        private BoardContext context;

        public ListService()
        {
            context = new BoardContext();
        }

        public void Create(ListEntity list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            context.Set<ListEntity>().Add(list);

            context.SaveChanges();
        }

        public ListEntity Read(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), "Argument was empty.");
            }

            var list = context.Set<ListEntity>()
                .Include("Cards")
                .AsNoTracking()
                .FirstOrDefault(lst => lst.Id == id);

            if (list == null)
            {
                throw new ObjectNotFoundException("Nothing was found by given id.");
            }

            return list;
        }

        public IEnumerable<ListEntity> Read()
        {
            var lists = context.Set<ListEntity>().AsNoTracking().ToList();

            if (!lists.Any())
            {
                throw new ObjectNotFoundException("Empty enumeration returned.");
            }

            return lists;
        }

        public void Update(ListEntity list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            context.Entry(list).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), "Argument was empty.");
            }

            var list = context.Set<ListEntity>().Find(id);

            if (list == null)
            {
                throw new ObjectNotFoundException("Nothing was found by given id.");
            }

            context.Set<ListEntity>().Remove(list);

            context.SaveChanges();
        }

        public void Delete(ListEntity list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            context.Entry(list).State = EntityState.Deleted;

            context.SaveChanges();
        }
    }
}