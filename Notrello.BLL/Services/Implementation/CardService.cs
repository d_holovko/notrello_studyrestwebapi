﻿namespace Notrello.BLL.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core;
    using System.Linq;

    using Notrello.DAL;
    using Notrello.DAL.Model;

    public class CardService : ICardService
    {
        private BoardContext context;

        public CardService()
        {
            context = new BoardContext();
        }

        public void Create(CardEntity card)
        {
            if (card == null)
            {
                throw new ArgumentNullException(nameof(card));
            }

            context.Set<CardEntity>().Add(card);

            context.SaveChanges();
        }

        public CardEntity Read(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), "Argument was empty.");
            }

            var card = context.Set<CardEntity>().AsNoTracking().FirstOrDefault(crd => crd.Id == id);

            if (card == null)
            {
                throw new ObjectNotFoundException("Nothing was found by given id.");
            }

            return card;
        }

        public IEnumerable<CardEntity> Read()
        {
            var cards = context.Set<CardEntity>().AsNoTracking().ToList();

            if (!cards.Any())
            {
                throw new ObjectNotFoundException("Empty enumeration returned.");
            }

            return cards;
        }

        public void Update(CardEntity card)
        {
            if (card == null)
            {
                throw new ArgumentNullException(nameof(card));
            }

            context.Entry(card).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), "Argument was empty.");
            }

            var card = context.Set<CardEntity>().Find(id);

            if (card == null)
            {
                throw new ObjectNotFoundException("Nothing was found by given id.");
            }

            context.Set<CardEntity>().Remove(card);

            context.SaveChanges();
        }

        public void Delete(CardEntity card)
        {
            if (card == null)
            {
                throw new ArgumentNullException(nameof(card));
            }

            context.Entry(card).State = EntityState.Deleted;

            context.SaveChanges();
        }
    }
}
