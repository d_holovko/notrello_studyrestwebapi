﻿namespace Notrello.BLL.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core;
    using System.Linq;

    using Notrello.DAL;
    using Notrello.DAL.Model;

    public class BoardService : IBoardService
    {
        private readonly BoardContext context;

        public BoardService()
        {
            context = new BoardContext();
        }
        
        public void Create(BoardEntity board)
        {
            if (board == null)
            {
                throw new ArgumentNullException(nameof(board));
            }

            context.Set<BoardEntity>().Add(board);

            context.SaveChanges();
        }

        public BoardEntity Read(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), "Argument was empty.");
            }

            var board = context.Set<BoardEntity>()
                .Include("Lists")
                .Include("Lists.Cards")
                .AsNoTracking()
                .FirstOrDefault(brd => brd.Id == id);

            if (board == null)
            {
                throw new ObjectNotFoundException("Nothing was found by given id.");
            }

            return board;
        }

        public IEnumerable<BoardEntity> Read()
        {
            var boards = context.Set<BoardEntity>().AsNoTracking().ToList();

            if (!boards.Any())
            {
                throw new ObjectNotFoundException("Empty enumeration returned.");
            }

            return boards;
        }

        public void Update(BoardEntity board)
        {
            if (board == null)
            {
                throw new ArgumentNullException(nameof(board));
            }

            context.Entry(board).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(id), "Argument was empty.");
            }

            var board = context.Set<BoardEntity>().Find(id);

            if (board == null)
            {
                throw new ObjectNotFoundException("Nothing was found by given id.");
            }

            context.Set<BoardEntity>().Remove(board);

            context.SaveChanges();
        }

        public void Delete(BoardEntity board)
        {
            if (board == null)
            {
                throw new ArgumentNullException(nameof(board));
            }

            context.Entry(board).State = EntityState.Deleted;

            context.SaveChanges();
        }
    }
}