﻿namespace Notrello.BLL.Services
{
    using System;
    using System.Collections.Generic;

    using Notrello.DAL.Model;

    public interface IBoardService
    {
        void Create(BoardEntity board);

        BoardEntity Read(Guid id);

        IEnumerable<BoardEntity> Read();

        void Update(BoardEntity board);

        void Delete(Guid id);

        void Delete(BoardEntity board);
    }
}