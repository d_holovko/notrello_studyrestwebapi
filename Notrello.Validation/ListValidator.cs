﻿namespace Notrello.Validation
{
    using System;

    using FluentValidation;

    using Notrello.DAL.Model;

    public class ListValidator : AbstractValidator<ListEntity>
    {
        public ListValidator()
        {
            RuleSet("Full",
                () => {
                    RuleFor(lst => lst.Id).NotNull().NotEmpty();
                });

            RuleSet("Full, Creation",
                () => { 
                    RuleFor(lst => lst.BoardId).NotNull().NotEmpty();
                    RuleFor(lst => lst.Name).NotEmpty();
                });
        }
    }
}