﻿namespace Notrello.Validation
{
    using System;

    using FluentValidation;

    using Notrello.DAL.Model;

    public class BoardValidator : AbstractValidator<BoardEntity>
    {
        public BoardValidator()
        {
            RuleSet("Full",
                () => {
                    RuleFor(brd => brd.Id).NotNull().NotEmpty();
                });

            RuleSet("Full, Creation",
                () => {
                    RuleFor(brd => brd.Name).NotEmpty();
                });
        }
    }
}
