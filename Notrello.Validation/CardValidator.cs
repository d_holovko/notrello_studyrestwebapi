﻿namespace Notrello.Validation
{
    using System;

    using FluentValidation;

    using Notrello.DAL.Model;

    public class CardValidator : AbstractValidator<CardEntity>
    {
        public CardValidator()
        {
            RuleSet("Full", 
                () => {
                    RuleFor(crd => crd.Id).NotNull().NotEmpty();
                });

            RuleSet("Full, Creation",
                () => {
                    RuleFor(crd => crd.ListId).NotNull().NotEmpty();
                    RuleFor(crd => crd.Text).NotEmpty();
                });
        }
    }
}
